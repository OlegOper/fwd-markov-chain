#include "tools.h"

#include <algorithm>

namespace textgen {

RandomSelector::RandomSelector(): randEngine_(randomDevice_()) {
}

RandomSelector::DistributionRange RandomSelector::makeRange(const std::unordered_map<int32_t, int>& stats) {
    DistributionRange res;
    res.reserve(stats.size());
    int sum = std::accumulate(stats.begin(), stats.end(), 0, [] (int sum, auto it) { return sum + it.second; });

    for (const auto& it : stats) {
        res.emplace_back(static_cast<double>(it.second) / sum, it.first);
    }
    std::sort(res.begin(), res.end(), [] (const auto& a, const auto& b) { return a.first < b.first; });
    double current = 0;
    for (auto& it : res) {
        it.first += current;
        current = it.first;
    }
    return res;
}

int32_t RandomSelector::pickAtRandom(DistributionRange& range) {
    double value = nextRandom();
    const auto it = std::upper_bound(range.begin(), range.end(), value, [] (const auto& a, const auto& b) {
        return a < b.first;
    });
    if (it == range.end()) {
        return range.back().second;
    }
    return it->second;
}

double RandomSelector::nextRandom() {
    std::uniform_real_distribution<double> uniform(0, 1);
    return uniform(randEngine_);
}

} //namespace textgen
