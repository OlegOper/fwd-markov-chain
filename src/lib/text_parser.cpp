#include "text_parser.h"

#include <boost/locale.hpp>

#include <algorithm>
#include <iterator>
#include <sstream>

namespace textgen {

TextParser::TextParser(const std::string& text) {
    const auto& words = splitWords(text);
    for (const auto& it : words) {
        const std::string& normalized = normalizeWord(it);
        if (!normalized.empty()) {
            words_.push_back(normalized);
        }
    }
}

std::vector<std::string> TextParser::splitWords(const std::string& text) {
    std::string clearText = text;
    auto pred = [](char c) {
        return std::isprint(c) && !std::isalnum(c);

    };
    std::replace_if(clearText.begin(), clearText.end(), pred, ' ');
    std::istringstream iss(clearText);
    std::vector<std::string> res(std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>());
    return res;
}

std::string TextParser::normalizeWord(const std::string& w) {
    std::string res = boost::locale::to_lower(w);
    return res;
}

} // namespace textgen
