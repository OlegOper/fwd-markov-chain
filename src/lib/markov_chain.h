#pragma once

#include <stdint.h>
#include <deque>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include "tools.h"

namespace textgen {

class MarkovChainModel {
public:
    MarkovChainModel(
        int size,
        const std::unordered_map<std::deque<int32_t>, RandomSelector::DistributionRange, DequeHasher>& model);

    MarkovChainModel(std::istream& from) {
        deserialize(from);
    }

    int getModelSize() {
        return size_;
    }

    void addData(const std::deque<int32_t>& state, const RandomSelector::DistributionRange& next);

    void serialize(std::ostream& to);

    bool getNext(const std::deque<int32_t>& state, int32_t& next);

private:
    void deserialize(std::istream& from);

    int deserializeInt(std::istream& from);

    void serializeSingle(std::ostream& to, const std::deque<int32_t>& state, const RandomSelector::DistributionRange& value);

    void deserializeSingle(int size, const std::string& line);

    void serializeDistributionRange(std::ostream& to, const RandomSelector::DistributionRange& value);
    RandomSelector::DistributionRange deserializeDistributionRange(std::istream& from);

    int size_;
    std::unordered_map<std::deque<int32_t>, RandomSelector::DistributionRange, DequeHasher> model_;
    RandomSelector randomSelector_;
};

class MarkovChainModelBuilder {
public:
    MarkovChainModelBuilder(int size);

    void train(int32_t nextWord);

    void resetState() {
        state_ = {};
    }

    std::unique_ptr<MarkovChainModel> build();

private:
    const int size_;
    std::deque<int32_t> state_;
    std::unordered_map<std::deque<int32_t>, std::unordered_map<int32_t, int>, DequeHasher> model_;
};

class MarkovChainGenerator {
public:
    MarkovChainGenerator(std::unique_ptr<MarkovChainModel>&& model, const std::deque<int32_t> state);
    bool generate(int32_t& next);

private:
    std::deque<int32_t> state_;
    std::unique_ptr<MarkovChainModel> model_;
};

} // namespace textgen
